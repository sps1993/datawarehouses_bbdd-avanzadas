package Presentaci�n;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.CardLayout;
import javax.swing.SpringLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import Dominio.ConexionBD;
import Persistencia.DBGestionCCAA;
import Persistencia.DBGestionFecha;
import Persistencia.DBGestionHechos;
import Persistencia.DBGestionSector;
import Persistencia.DataBase;
import Persistencia.ETL;

import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Vector;

public class Interface {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface window = new Interface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//Comprueba si hay db y si no la crea
		DataBase db=new DataBase();
		db.exist();
		//extrae, transforma y carga datos
		ETL ext=new ETL();
		ext.cargaDatos(ext.transformacionDatos(ext.extraccionDatos()));
		
		DBGestionCCAA cA=new DBGestionCCAA();
		LinkedList<String[]>cAs=cA.readAll();
		
		DBGestionSector sector=new DBGestionSector();
		LinkedList<String[]>sectores=sector.readAll();
		
		DBGestionFecha fechas=new DBGestionFecha();
		LinkedList<String[]>dates=fechas.readAll();
		
		DBGestionHechos hechos=new DBGestionHechos();
		
		frame = new JFrame();
		frame.setBounds(100, 100, 1002, 623);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new MigLayout("", "[][][][grow][][][][][][][][][][][grow][][][][][][][][][][][grow][][][][][][][][]", "[][][]"));
		
		JLabel lblAo = new JLabel("A\u00F1o");
		panel.add(lblAo, "cell 1 1 2 1");
		
		JComboBox comboBox = new JComboBox();
		comboBox.addItem("TODOS");
		for(int i=0; i<dates.size();i++){
			comboBox.addItem(dates.get(i)[1]);
		}
		panel.add(comboBox, "cell 3 1 6 1,growx");
		
		JLabel lblSector = new JLabel("Sector");
		panel.add(lblSector, "cell 10 1 4 1");
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.addItem("TODOS");
		for(int i=0; i<sectores.size();i++){
			comboBox_1.addItem(sectores.get(i)[1]);
		}
		panel.add(comboBox_1, "cell 14 1 7 1,growx");
		
		JLabel lblComunidadAutonoma = new JLabel("Comunidad Autonoma");
		panel.add(lblComunidadAutonoma, "cell 22 1 3 1");
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.addItem("TODOS");
		for(int i=0; i<cAs.size();i++){
			comboBox_2.addItem(cAs.get(i)[1]);
		}
		panel.add(comboBox_2, "cell 25 1 8 1,growx");
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.WEST);
		panel_1.setLayout(new MigLayout("", "[][][][][grow][]", "[][][][][][][][][][][][][][][][][grow]"));
		
		JLabel lblLeyenda = new JLabel("Datos A Mostrar:");
		lblLeyenda.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblLeyenda, "cell 0 0 5 2,grow");
		
		JCheckBox chckbxGastosInternosmiles = new JCheckBox("Gastos Internos (miles)");
		chckbxGastosInternosmiles.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}
		});
		panel_1.add(chckbxGastosInternosmiles, "cell 1 2 4 1,grow");
		
		JCheckBox chckbxGastosInternos = new JCheckBox("Gastos Internos (%)");
		panel_1.add(chckbxGastosInternos, "cell 1 3 4 1,grow");
		
		JCheckBox chckbxPersonalIdhombres = new JCheckBox("Personal I+D (hombres)");
		panel_1.add(chckbxPersonalIdhombres, "cell 1 4 4 1,grow");
		
		JCheckBox chckbxPersonalIdmujeres = new JCheckBox("Personal I+D (mujeres)");
		panel_1.add(chckbxPersonalIdmujeres, "cell 1 5 4 1,grow");
		
		JCheckBox chckbxPersonalIdtotal = new JCheckBox("Personal I+D (total)");
		panel_1.add(chckbxPersonalIdtotal, "cell 1 6 4 1,grow");
		
		JCheckBox chckbxInvestigadoresIdhombres = new JCheckBox("Investigadores I+D (hombres)");
		panel_1.add(chckbxInvestigadoresIdhombres, "cell 1 7 4 1,grow");
		
		JCheckBox chckbxInvestigadoresIdmujeres = new JCheckBox("Investigadores I+D (mujeres)");
		panel_1.add(chckbxInvestigadoresIdmujeres, "cell 1 8 4 1,grow");
		
		JCheckBox chckbxInvestigadoresIdtotal = new JCheckBox("Investigadores I+D (total)");
		panel_1.add(chckbxInvestigadoresIdtotal, "cell 1 9 4 1,grow");
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		

		JButton btnMostrarInformacion = new JButton("Mostrar Informacion");
		btnMostrarInformacion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				DefaultTableModel modelo= new DefaultTableModel();
				String busqueda="";
				table.setModel(modelo);
				modelo.addColumn("A�o");
				modelo.addColumn("Sector");
				modelo.addColumn("Comunidad Autonoma");
				LinkedList<String[]>lista=new LinkedList<String[]>();
				ResultSet res;
				String[]el=new String[9];
				if((comboBox.getSelectedItem()+"").equalsIgnoreCase("TODOS")&&(comboBox_1.getSelectedItem()+"").equalsIgnoreCase("TODOS")&&(comboBox_2.getSelectedItem()+"").equalsIgnoreCase("TODOS")){
					res=hechos.sentenciaEspecial("SELECT idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos");
					try {
						while(res.next()){
							el=new String[9];
							el[0]=fechas.getNombre(res.getInt("idYear"));
							el[1]=sector.getNombre(res.getInt("idSector"));
							el[2]=cA.getNombre(res.getInt("idCCAA"));
							el[3]=""+res.getDouble("gIM");
							el[4]=""+res.getDouble("gIP");
							el[5]=""+res.getDouble("pIDM");
							el[6]=""+res.getDouble("pIDT");
							el[7]=""+res.getDouble("iIDM");
							el[8]=""+res.getDouble("iIDT");;
							lista.add(el);
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}else{
					if((comboBox.getSelectedItem()+"").equalsIgnoreCase("TODOS")&&(comboBox_1.getSelectedItem()+"").equalsIgnoreCase("TODOS")){
						cA.getID(""+comboBox_2.getSelectedItem());
						res=hechos.sentenciaEspecial("SELECT idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idCCAA="+cA.getID(""+comboBox_2.getSelectedItem()));
						try {
							while(res.next()){
								el=new String[9];
								el[0]=fechas.getNombre(res.getInt("idYear"));
								el[1]=sector.getNombre(res.getInt("idSector"));
								el[2]=cA.getNombre(res.getInt("idCCAA"));
								el[3]=""+res.getDouble("gIM");
								el[4]=""+res.getDouble("gIP");
								el[5]=""+res.getDouble("pIDM");
								el[6]=""+res.getDouble("pIDT");
								el[7]=""+res.getDouble("iIDM");
								el[8]=""+res.getDouble("iIDT");
								lista.add(el);
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}else{
						if((comboBox.getSelectedItem()+"").equalsIgnoreCase("TODOS")&&(comboBox_2.getSelectedItem()+"").equalsIgnoreCase("TODOS")){
							res=hechos.sentenciaEspecial("SELECT idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idSector="+sector.getID(""+comboBox_1.getSelectedItem()));
							try {
								while(res.next()){
									el=new String[9];
									el[0]=fechas.getNombre(res.getInt("idYear"));
									el[1]=sector.getNombre(res.getInt("idSector"));
									el[2]=cA.getNombre(res.getInt("idCCAA"));
									el[3]=""+res.getDouble("gIM");
									el[4]=""+res.getDouble("gIP");
									el[5]=""+res.getDouble("pIDM");
									el[6]=""+res.getDouble("pIDT");
									el[7]=""+res.getDouble("iIDM");
									el[8]=""+res.getDouble("iIDT");
									lista.add(el);
								}
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}else{
							if((comboBox_1.getSelectedItem()+"").equalsIgnoreCase("TODOS")&&(comboBox_2.getSelectedItem()+"").equalsIgnoreCase("TODOS")){
								res=hechos.sentenciaEspecial("SELECT idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idYear="+fechas.getID(Integer.parseInt(""+comboBox.getSelectedItem())));
								try {
									while(res.next()){
										el=new String[9];
										el[0]=fechas.getNombre(res.getInt("idYear"));
										el[1]=sector.getNombre(res.getInt("idSector"));
										el[2]=cA.getNombre(res.getInt("idCCAA"));
										el[3]=""+res.getDouble("gIM");
										el[4]=""+res.getDouble("gIP");
										el[5]=""+res.getDouble("pIDM");
										el[6]=""+res.getDouble("pIDT");
										el[7]=""+res.getDouble("iIDM");
										el[8]=""+res.getDouble("iIDT");
										lista.add(el);
									}
								} catch (SQLException e) {
									e.printStackTrace();
								}
							}else{
								if((comboBox.getSelectedItem()+"").equalsIgnoreCase("TODOS")){
									res=hechos.sentenciaEspecial("SELECT idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idSector="+sector.getID(comboBox_1.getSelectedItem()+"")+" AND idCCAA="+cA.getID(comboBox_2.getSelectedItem()+""));
									try {
										while(res.next()){
											el=new String[9];
											el[0]=fechas.getNombre(res.getInt("idYear"));
											el[1]=sector.getNombre(res.getInt("idSector"));
											el[2]=cA.getNombre(res.getInt("idCCAA"));
											el[3]=""+res.getDouble("gIM");
											el[4]=""+res.getDouble("gIP");
											el[5]=""+res.getDouble("pIDM");
											el[6]=""+res.getDouble("pIDT");
											el[7]=""+res.getDouble("iIDM");
											el[8]=""+res.getDouble("iIDT");
											lista.add(el);
										}
									} catch (SQLException e) {
										e.printStackTrace();
									}
								}else{
									if((comboBox_1.getSelectedItem()+"").equalsIgnoreCase("TODOS")){
										res=hechos.sentenciaEspecial("SELECT idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idYear="+fechas.getID(Integer.parseInt(comboBox.getSelectedItem()+""))+" AND idCCAA="+cA.getID(comboBox_2.getSelectedItem()+""));
										try {
											while(res.next()){
												el=new String[9];
												el[0]=fechas.getNombre(res.getInt("idYear"));
												el[1]=sector.getNombre(res.getInt("idSector"));
												el[2]=cA.getNombre(res.getInt("idCCAA"));
												el[3]=""+res.getDouble("gIM");
												el[4]=""+res.getDouble("gIP");
												el[5]=""+res.getDouble("pIDM");
												el[6]=""+res.getDouble("pIDT");
												el[7]=""+res.getDouble("iIDM");
												el[8]=""+res.getDouble("iIDT");
												lista.add(el);
											}
										} catch (SQLException e) {
											e.printStackTrace();
										}
									}else{
										if((comboBox_2.getSelectedItem()+"").equalsIgnoreCase("TODOS")){
											res=hechos.sentenciaEspecial("SELECT idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idYear="+fechas.getID(Integer.parseInt(comboBox.getSelectedItem()+""))+" AND idSector="+sector.getID(comboBox_1.getSelectedItem()+""));
											try {
												while(res.next()){
													el=new String[9];
													el[0]=fechas.getNombre(res.getInt("idYear"));
													el[1]=sector.getNombre(res.getInt("idSector"));
													el[2]=cA.getNombre(res.getInt("idCCAA"));
													el[3]=""+res.getDouble("gIM");
													el[4]=""+res.getDouble("gIP");
													el[5]=""+res.getDouble("pIDM");
													el[6]=""+res.getDouble("pIDT");
													el[7]=""+res.getDouble("iIDM");
													el[8]=""+res.getDouble("iIDT");
													lista.add(el);
												}
											} catch (SQLException e) {
												e.printStackTrace();
											}
										}else{
											res=hechos.sentenciaEspecial("SELECT idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idYear="+fechas.getID(Integer.parseInt(comboBox.getSelectedItem()+""))+" AND idSector="+sector.getID(comboBox_1.getSelectedItem()+"")+" AND idCCAA="+cA.getID(comboBox_2.getSelectedItem()+""));
											try {
												while(res.next()){
													el=new String[9];
													el[0]=fechas.getNombre(res.getInt("idYear"));
													el[1]=sector.getNombre(res.getInt("idSector"));
													el[2]=cA.getNombre(res.getInt("idCCAA"));
													el[3]=""+res.getDouble("gIM");
													el[4]=""+res.getDouble("gIP");
													el[5]=""+res.getDouble("pIDM");
													el[6]=""+res.getDouble("pIDT");
													el[7]=""+res.getDouble("iIDM");
													el[8]=""+res.getDouble("iIDT");
													lista.add(el);
												}
											} catch (SQLException e) {
												e.printStackTrace();
											}
										}
									}
								}
							}
						}
					}
				}
				LinkedList<LinkedList<String>>listaFinal=new LinkedList<LinkedList<String>>();
				String[]elem;
				Vector v;
				if(chckbxGastosInternosmiles.isSelected()){
					modelo.addColumn("Gastos Internos (miles)");
				}
				if(chckbxGastosInternos.isSelected()){
					modelo.addColumn("Gastos Internos (%)");
				}
				if(chckbxPersonalIdhombres.isSelected()){
					modelo.addColumn("Personal I+D (hombres)");
				}
				if(chckbxPersonalIdmujeres.isSelected()){
					modelo.addColumn("Personal I+D (mujeres)");
				}
				if(chckbxPersonalIdtotal.isSelected()){
					modelo.addColumn("Personal I+D (total)");
				}
				if(chckbxInvestigadoresIdhombres.isSelected()){
					modelo.addColumn("Investigadores I+D (hombres)");
				}
				if(chckbxInvestigadoresIdmujeres.isSelected()){
					modelo.addColumn("Investigadores I+D (mujeres)");
				}
				if(chckbxInvestigadoresIdtotal.isSelected()){
					modelo.addColumn("Investigadores I+D (total)");
				}
				for(int i=0; i<lista.size(); i++){
					elem=lista.get(i);
					v=new Vector();
					v.add(elem[0]);
					v.add(elem[1]);
					v.add(elem[2]);
					if(chckbxGastosInternosmiles.isSelected()){
						if(elem[3].equalsIgnoreCase("-1.0")){
							elem[3]="unk";
						}
						v.add(elem[3]);
					}
					if(chckbxGastosInternos.isSelected()){
						if(elem[4].equalsIgnoreCase("-1.0")){
							elem[4]="unk";
						}
						v.add(elem[4]);
					}
					if(chckbxPersonalIdhombres.isSelected()){
						if(elem[6].equalsIgnoreCase("-1.0")&&elem[5].equalsIgnoreCase("-1.0")){
							v.add("unk");
						}else{
							v.add(""+(Double.parseDouble(elem[6])-Double.parseDouble(elem[5])));
						}
					}
					if(chckbxPersonalIdmujeres.isSelected()){
						if(elem[5].equalsIgnoreCase("-1.0")){
							elem[5]="unk";
						}
						v.add(elem[5]);
					}
					if(chckbxPersonalIdtotal.isSelected()){
						if(elem[6].equalsIgnoreCase("-1.0")){
							elem[6]="unk";
						}
						v.add(elem[6]);
					}
					if(chckbxInvestigadoresIdhombres.isSelected()){
						if(elem[7].equalsIgnoreCase("-1.0")&&elem[8].equalsIgnoreCase("-1.0")){
							v.add("unk");
						}else{
							v.add(""+(Double.parseDouble(elem[8])-Double.parseDouble(elem[7])));
						}
					}
					if(chckbxInvestigadoresIdmujeres.isSelected()){
						if(elem[7].equalsIgnoreCase("-1.0")){
							elem[7]="unk";
						}
						v.add(elem[7]);
					}
					if(chckbxInvestigadoresIdtotal.isSelected()){
						if(elem[8].equalsIgnoreCase("-1.0")){
							elem[8]="unk";
						}
						v.add(elem[8]);
					}
					modelo.addRow(v);
				}
			}
		});
		panel_1.add(btnMostrarInformacion, "cell 2 11 3 1,grow");
	}

}
