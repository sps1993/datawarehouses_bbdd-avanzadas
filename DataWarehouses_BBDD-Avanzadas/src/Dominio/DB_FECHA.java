package Dominio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;


public class DB_FECHA {
	ConexionBD mInstancia;
	ResultSet res;
	PreparedStatement preparedStmt;
	Connection conn;
	public DB_FECHA(){		
		mInstancia=ConexionBD.getInstance();
	}
	public void create(int year){
		mInstancia.sentenciaInsert("INSERT INTO dwestadisticaactividades.dimension_fecha (idYear, year) VALUES ("+(this.nElementos()+1)+","+year+")");
	}
	
	public int update(int year, int idFecha){
		return mInstancia.sentenciaUpdate("UPDATE dimension_fecha SET year="+year+" WHERE idFecha="+idFecha);
	}
	
	public int read(int year){
		try {
			res=mInstancia.sentenciaSelect("SELECT idFecha FROM dimension_fecha WHERE year="+year+"");
			if(res.next()){
				int id=Integer.parseInt(res.getString("idFecha"));
				return id;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean exist(int year){
		try {
			res=mInstancia.sentenciaSelect("SELECT count(idYear) as total FROM dwestadisticaactividades.dimension_fecha WHERE year="+year+";");
			if (res==null) return false;
			if(res.next()){
				if (res.getInt("total")==1) return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public int getID(int year){
		try {
			res=mInstancia.sentenciaSelect("SELECT idYear FROM dwestadisticaactividades.dimension_fecha WHERE year="+year+";");
			if(res.next()){
				return (res.getInt("idYear"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public String getNombre(int id){
		try {
			res=mInstancia.sentenciaSelect("SELECT year FROM dwestadisticaactividades.dimension_fecha WHERE idYear="+id+";");
			if(res.next()){
				return (res.getString("Year"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public LinkedList<String[]> readAll(){
		LinkedList<String[]>con=new LinkedList<String[]>();
		try {
			String[]el;
			res = mInstancia.sentenciaSelect("SELECT idYear, year FROM dimension_fecha WHERE idYear>0");
			while(res.next()){
				el=new String[2];
				el[0]=res.getString("idYear");
				el[1]=res.getString("year").substring(0, 4);
				con.add(el);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	public int nElementos(){
		try {
			res=mInstancia.sentenciaSelect("SELECT count(idYear) as total FROM dwestadisticaactividades.dimension_fecha");
			if(res==null) return 0;
			if(res.next()) 
				return Integer.parseInt(res.getString("total")); 
			else 
				return 0;
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (SQLException e2) {
			e2.printStackTrace();
		} 
		return 0;
	}
}