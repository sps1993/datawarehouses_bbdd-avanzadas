package Dominio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;


public class DB_CCAA {
	ConexionBD mInstancia;
	ResultSet res;
	PreparedStatement preparedStmt;
	Connection conn;
	public DB_CCAA(){		
		mInstancia=ConexionBD.getInstance();
	}
	public void create(String ccAA){
		mInstancia.sentenciaInsert("INSERT INTO dwestadisticaactividades.dimension_ccaa (idCCAA, ccaa) VALUES ("+(this.nElementos()+1)+",'"+ccAA+"')");
	}
	public int read(String ccAA){
		try {
			res=mInstancia.sentenciaSelect("SELECT idCCAA FROM dwestadisticaactividades.dimension_ccaa WHERE ccaa='"+ccAA+"'");
			if(res.next()){
				int id=Integer.parseInt(res.getString("idCCAA"));
				return id;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public boolean exist(String nC){
		try {
			res=mInstancia.sentenciaSelect("SELECT count(idCCAA) as total FROM dwestadisticaactividades.dimension_ccaa WHERE ccaa='"+nC+"';");
			if(res.next()){
				if (res.getInt("total")==1) return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public int getID(String nC){
		try {
			res=mInstancia.sentenciaSelect("SELECT idCCAA FROM dwestadisticaactividades.dimension_ccaa WHERE ccaa='"+nC+"';");
			if(res.next()){
				return (res.getInt("idCCAA"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public String getNombre(int id){
		try {
			res=mInstancia.sentenciaSelect("SELECT ccAA FROM dwestadisticaactividades.dimension_ccaa WHERE idCCAA="+id+";");
			if(res.next()){
				return (res.getString("ccAA"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public LinkedList<String[]> readAll(){
		LinkedList<String[]>con=new LinkedList<String[]>();
		try {
			String[]el;
			res=mInstancia.sentenciaSelect("SELECT idCCAA, ccaa FROM dwestadisticaactividades.dimension_ccaa WHERE idCCAA>0");
			while(res.next()){
				el=new String[2];
				el[0]=res.getString("idCCAA");
				el[1]=res.getString("ccaa");
				con.add(el);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}

	public int nElementos(){
		try {
			res=mInstancia.sentenciaSelect("SELECT count(idCCAA) as total FROM dwestadisticaactividades.dimension_CCAA");
			if(res.next()) 
				return Integer.parseInt(res.getString("total")); 
			else 
				return 0;
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (SQLException e2) {
			e2.printStackTrace();
		} 
		return 0;
	}
}