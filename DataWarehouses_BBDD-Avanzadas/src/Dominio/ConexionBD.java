package Dominio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexionBD {
	static ConexionBD mInstancia;
	Connection conn = null;
    Statement stmt = null;
    final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	//final static String DB_URL = "jdbc:mysql://127.0.0.1:3306/dwEstadisticaActividades?autoReconnect=true&useSSL=false";
    final static String DB_URL = "jdbc:mysql://127.0.0.1:3306/?autoReconnect=true&useSSL=false";
	final String USER = "root";
	final String PASS = "root";    
	ResultSet rs;
	public static ConexionBD getInstance(){
		if(mInstancia==null){
			mInstancia=new ConexionBD();
		}
		return mInstancia;
	}
	public Connection conectar(){
		try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            return conn;
        } catch (ClassNotFoundException e1) {
        	e1.printStackTrace();
        } catch (SQLException e2) {
            e2.printStackTrace();
        }catch(Exception e3){
	    	e3.printStackTrace();
	    }
		return null;
	}
	public void desconectar(){
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int sentenciaInsert(String sentencia){
		try {
			stmt=conn.createStatement();
			return stmt.executeUpdate(sentencia);
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public ResultSet sentenciaSelect(String sentencia){
		try {
			stmt=conn.createStatement();
			return stmt.executeQuery(sentencia);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public int sentenciaUpdate(String sentencia){
		try {
			stmt=conn.createStatement();
			return stmt.executeUpdate(sentencia);
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public void crearDB(){
		try{
			stmt=conn.createStatement();
			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS dwEstadisticaActividades;");
			stmt.executeUpdate("USE dwEstadisticaActividades;");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS dimension_fecha(idYear int NOT NULL, year int, PRIMARY KEY(idYear));");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS dimension_sector(idSector int NOT NULL, sector VARCHAR(255), PRIMARY KEY (idSector));");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS dimension_ccaa(idCCAA int NOT NULL, ccaa VARCHAR(255), PRIMARY KEY (idCCAA));");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS tabla_hechos(idYear int, idSector int, idCCAA int, FOREIGN KEY (idYear) REFERENCES dimension_fecha(idYear), FOREIGN KEY (idSector) REFERENCES dimension_sector(idSector), FOREIGN KEY (idCCAA) REFERENCES dimension_ccaa(idCCAA), PRIMARY KEY (idYear, idSector, idCCAA), gIM double, gIP double, pIDT double, pIDM double, iIDT double, iIDM double);");
		}catch(SQLException ex){
			ex.printStackTrace();
		}
	}
}
