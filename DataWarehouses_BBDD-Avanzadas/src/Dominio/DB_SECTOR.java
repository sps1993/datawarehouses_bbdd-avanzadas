package Dominio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;


public class DB_SECTOR {
	ConexionBD mInstancia;
	ResultSet res;
	PreparedStatement preparedStmt;
	Connection conn;
	public DB_SECTOR(){		
		mInstancia=ConexionBD.getInstance();
	}
	public void create(String sector){
		mInstancia.sentenciaInsert("INSERT INTO dwestadisticaactividades.dimension_sector (idSector, sector) VALUES ("+(this.nElementos()+1)+",'"+sector+"')");
	}
	public int read(String sector){
		try {
			res=mInstancia.sentenciaSelect("SELECT idSector FROM dwestadisticaactividades.dimension_sector WHERE nombresector='"+sector+"'");
			if(res.next()){
				int id=Integer.parseInt(res.getString("idSector"));
				return id;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public LinkedList<String[]> readAll(){
		LinkedList<String[]>con=new LinkedList<String[]>();
		try {
			String[]el;
			res=mInstancia.sentenciaSelect("SELECT idSector, sector FROM dwestadisticaactividades.dimension_sector WHERE idSector>0");
			while(res.next()){
				el=new String[2];
				el[0]=res.getString("idSector");
				el[1]=res.getString("sector");
				con.add(el);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}

	public boolean exist(String sector){
		try {
			res=mInstancia.sentenciaSelect("SELECT count(idSector) as total FROM dwestadisticaactividades.dimension_sector WHERE sector='"+sector+"';");
			if(res.next()){
				if (res.getInt("total")==1) return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public int getID(String sector){
		try {
			res=mInstancia.sentenciaSelect("SELECT idSector FROM dwestadisticaactividades.dimension_sector WHERE sector='"+sector+"';");
			if(res.next()){
				return (res.getInt("idSector"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public String getNombre(int id){
		try {
			res=mInstancia.sentenciaSelect("SELECT sector FROM dwestadisticaactividades.dimension_sector WHERE idSector="+id+";");
			if(res.next()){
				return (res.getString("sector"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public int nElementos(){
		try {
			res=mInstancia.sentenciaSelect("SELECT count(idSector) as total FROM dwestadisticaactividades.dimension_sector");
			if(res.next()) 
				return Integer.parseInt(res.getString("total")); 
			else 
				return 0;
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (SQLException e2) {
			e2.printStackTrace();
		} 
		return 0;
	}
}