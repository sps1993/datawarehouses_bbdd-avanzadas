package Dominio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;


public class DB_HECHOS {
	ConexionBD mInstancia;
	ResultSet res;
	PreparedStatement preparedStmt;
	Connection conn;
	public DB_HECHOS(){		
		mInstancia=ConexionBD.getInstance();
	}
	public void create(int idAnno, int idSectorID, int cA, double mGI, double pGI, double pIDT, double pIDM, double iIDT, double iIDM){
		mInstancia.sentenciaInsert("INSERT INTO tabla_hechos (idYear, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM) VALUES ("+idAnno+","+idSectorID+","+cA+","+mGI+","+pGI+","+pIDM+","+pIDT+","+iIDM+","+iIDT+")");
	}
	
	public ResultSet sentenciaEspecial(String sentencia){
		return mInstancia.sentenciaSelect(sentencia);
	}
	
	public String[] read(int idAnno, int idSector, int idComunidad){
		try {
			String[]el=new String[6];
			res=mInstancia.sentenciaSelect("SELECT gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idYear="+idAnno+", idSector="+idSector+", idCCAA="+idComunidad);
			if(res.next()){
				el[0]=res.getString("gIM");
				el[1]=res.getString("gIP");
				el[2]=res.getString("pIDT");
				el[3]=res.getString("pIDM");
				el[4]=res.getString("iIDT");
				el[5]=res.getString("iIDM");
			}
			return el;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public LinkedList<String[]> readAll(){
		try {
			LinkedList<String[]>conj=new LinkedList<String[]>();
			String[]el=new String[6];
			res=mInstancia.sentenciaSelect("SELECT gIM, gIP, pIDT, pIDM, iIDT, iIDM FROM tabla_hechos WHERE idYear>0");
			while(res.next()){
				el[0]=res.getString("gIM");
				el[1]=res.getString("gIP");
				el[2]=res.getString("pIDT");
				el[3]=res.getString("pIDM");
				el[4]=res.getString("iIDT");
				el[5]=res.getString("iIDM");
				conj.add(el);
			}
			return conj;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mInstancia.desconectar();
		return null;
	}
	
	public void update(int idAnno, int idSectorID, int cA, double mGI, double pGI, double pIDT, double pIDM, double iIDT, double iIDM) throws SQLException{
		mInstancia.sentenciaInsert("UPDATE dwestadisticaactividades.tabla_hechos SET gIM="+mGI+", gIP="+pGI+", pIDT="+pIDT+", pIDM="+pIDM+", iIDT="+iIDT+", iIDM="+iIDM+" WHERE idYear="+idAnno+" AND idSector="+idSectorID+" AND idCCAA="+cA+";");
	}
	
	public boolean exist(int a�o, int sector, int cA){
		try {
			res=mInstancia.sentenciaSelect("SELECT count(idYear) as total FROM dwestadisticaactividades.tabla_hechos WHERE idYear="+a�o+" AND idSector="+sector+" AND idCCAA="+cA+";");
			if(res.next()){
				if (res.getInt("total")==1) return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public int nElementos(){
		try {
			res=mInstancia.sentenciaSelect("SELECT count(idHechos) as total FROM dwestadisticaactividades.tabla_hechos");
			if(res.next()) 
				return Integer.parseInt(res.getString("total")); 
			else 
				return 0;
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (SQLException e2) {
			e2.printStackTrace();
		} 
		return 0;
	}
}