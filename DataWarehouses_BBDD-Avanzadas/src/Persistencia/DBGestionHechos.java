package Persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import Dominio.DB_HECHOS;

public class DBGestionHechos {
	DB_HECHOS h=new DB_HECHOS();
	public void add(int idAnno, int idSectorID, int cA, double mGI, double pGI, double pIDT, double pIDM, double iIDT, double iIDM){
		h.create(idAnno, idSectorID, cA, mGI, pGI, pIDT, pIDM, iIDT, iIDM);
	}
	public String[] read(int idAnno, int idSector, int idComunidad){
		return h.read(idAnno, idSector, idComunidad);
	}
	public LinkedList<String[]> readAll(){
		return h.readAll();
	}
	public boolean existe(int a�o, int sector, int cA){
		return h.exist(a�o, sector, cA);
	}
	public void actualiza(int idAnno, int idSectorID, int cA, double mGI, double pGI, double pIDT, double pIDM, double iIDT, double iIDM){
		try {
			h.update(idAnno, idSectorID, cA, mGI, pGI, pIDT, pIDM, iIDT, iIDM);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet sentenciaEspecial(String sentencia){
		return h.sentenciaEspecial(sentencia);
	}
}