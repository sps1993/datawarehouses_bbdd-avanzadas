package Persistencia;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;

public class ETL {
	public LinkedList<LinkedList<String>> extraccionDatos(){
		LinkedList<LinkedList<String>>listaGeneral=new LinkedList<LinkedList<String>>();
		LinkedList<String>lista;
		String sDirectorio = "src/Dominio/Ficheros";
		File f = new File(sDirectorio);
		File[] directorios = f.listFiles();
		String line;
		int indice;
		BufferedReader br;
		System.out.println("Extrayendo datos de los archivo....");
		try {
			for (int x=0;x<directorios.length;x++){
				f = new File(sDirectorio+"/"+directorios[x].getName());
				for(int j=0; j<f.listFiles().length; j++){
					indice=1;
					br =new BufferedReader(new FileReader(f.listFiles()[j].getPath()));
					lista=new LinkedList<String>();
					lista.add(directorios[x].getName());//a�o
					lista.add(br.readLine());//sector
					while ((indice==1)&&((line=br.readLine())!=null)) {
			            String [] fields = line.split(";");
			            if(fields[0].indexOf("Notas")!=-1){
			            	indice=2;
			            }else{
			            	if((fields[1].indexOf("Gastos")==-1)&&(fields[1].indexOf("periodopx")==-1)){
			            		lista.add(line);
			            	}
			            }
					}
					listaGeneral.add(lista);
				}
			}
		    System.out.println("Todos los datos extraidos correctamente");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaGeneral;
	}
	
	public LinkedList<LinkedList<String>> transformacionDatos(LinkedList<LinkedList<String>>lineas){
		LinkedList<LinkedList<String>>general=new LinkedList<LinkedList<String>>();
		LinkedList<String>line;
		System.out.println("\nTransformando los datos....");
		for(int i=0; i<lineas.size(); i++){
			line=new LinkedList<String>();
			for(int j=0; j<lineas.get(i).size();j++){
				line.add(lineas.get(i).get(j).replace("..", "-1"));
			}
			general.add(line);
		}
		System.out.println("Fin de la transformacion de los datos");
		return general;
	}
	
	public void cargaDatos(LinkedList<LinkedList<String>>lineas){
		DBGestionCCAA ca=new DBGestionCCAA();
		DBGestionFecha fecha=new DBGestionFecha();
		DBGestionHechos hechos=new DBGestionHechos();
		DBGestionSector sectores=new DBGestionSector();
		String sector, a�o, ccaa;
		String[]tokens;
		int idA�o, idSector, idCCAA;
		double gIM, gIP, pIDT, pIDM, iIDT, iIDM;
		System.out.println("\nCargando datos al almacen (puede tardar varios minutos)...");
		for(int i=0; i<lineas.size(); i++){
			a�o=lineas.get(i).get(0);
			if(!(fecha.existe(Integer.parseInt(a�o)))){
				fecha.create(Integer.parseInt(a�o));
				System.out.println("Crea");
			}
			idA�o=fecha.getID(Integer.parseInt(a�o));
			sector=lineas.get(i).get(1);
			if(!(sectores.existe(sector))){
				sectores.add(sector);
			}
			idSector=sectores.getID(sector);
			for(int j=2; j<lineas.get(i).size(); j++){
				tokens=lineas.get(i).get(j).split(";");
				ccaa=tokens[0];
				if(!(ca.existe(ccaa))){
					ca.add(ccaa);
				}
				idCCAA=ca.getID(ccaa);
				gIM=Double.parseDouble(tokens[1]);
				gIP=Double.parseDouble(tokens[2]);
				pIDT=Double.parseDouble(tokens[3]);
				pIDM=Double.parseDouble(tokens[4]);
				iIDT=Double.parseDouble(tokens[5]);
				iIDM=Double.parseDouble(tokens[6]);
				if(hechos.existe(idA�o, idSector, idCCAA)){
					hechos.actualiza(idA�o, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM);
				}else{
					hechos.add(idA�o, idSector, idCCAA, gIM, gIP, pIDT, pIDM, iIDT, iIDM);
				}
			}
		}
		System.out.println("Fin de la carga de datos");
	}
}
