package Persistencia;

import java.util.LinkedList;

import Dominio.DB_FECHA;

public class DBGestionFecha {
	DB_FECHA cF=new DB_FECHA();
	public void create(int year){
		cF.create(year);
	}
	public int read(int year){
		return cF.read(year);
	}
	public int update(int year, int idFecha){
		return cF.update(year, idFecha);
	}
	public LinkedList<String[]> readAll(){
		return cF.readAll();
	}
	public boolean existe(int year){
		return cF.exist(year);
	}
	public int getID(int year){
		return cF.getID(year);
	}
	public String getNombre(int id){
		return cF.getNombre(id);
	}
}