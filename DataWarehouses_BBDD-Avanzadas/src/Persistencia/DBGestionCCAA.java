package Persistencia;

import java.util.LinkedList;

import Dominio.DB_CCAA;

public class DBGestionCCAA {
	DB_CCAA cA=new DB_CCAA();
	public void add(String nombreComunidad){
		cA.create(nombreComunidad);
	}
	public int read(String ccaa){
		return cA.read(ccaa);
	}
	public LinkedList<String[]> readAll(){
		return cA.readAll();
	}
	public boolean existe(String nC){
		return cA.exist(nC);
	}
	public int getID(String nC){
		return cA.getID(nC);
	}
	public String getNombre(int id){
		return cA.getNombre(id);
	}
}
