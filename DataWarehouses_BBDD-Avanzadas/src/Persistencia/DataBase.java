package Persistencia;

import Dominio.ConexionBD;

public class DataBase {
	public void exist(){
		ConexionBD bd=ConexionBD.getInstance();
		bd.conectar();
		bd.crearDB();
	}
	public void desconectar(){
		ConexionBD bd=ConexionBD.getInstance();
		bd.desconectar();
	}
}
