package Persistencia;

import java.util.LinkedList;

import Dominio.DB_SECTOR;

public class DBGestionSector {
	DB_SECTOR s=new DB_SECTOR();
	public void add(String sector){
		s.create(sector);
	}
	public int read(String sector){
		return s.read(sector);
	}
	public LinkedList<String[]> readAll(){
		return s.readAll();
	}
	public boolean existe(String year){
		return s.exist(year);
	}
	public int getID(String year){
		return s.getID(year);
	}
	public String getNombre(int id){
		return s.getNombre(id);
	}
}
